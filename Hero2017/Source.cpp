//https://kwarwas@bitbucket.org/kwarwas/hero2017_2.git

#include <iostream>
#include <string>
#include <ctime>

using namespace std;

class RandomNumbers
{
public:
	static void randomize()
	{
		srand(time(nullptr));
		rand();
	}

	static int random(int min, int max)
	{
		return (double)rand() / RAND_MAX * (max - min + 1) + min;
	}
};

class Przedmiot
{
};

class Plecak
{
	int pojemnosc = 10;
	Przedmiot *przedmioty;

public:
	Plecak()
	{
		przedmioty = new Przedmiot[pojemnosc];
	}
};

class Postac
{
	Plecak plecak;
	string nazwa;
	int sila;
	int zrecznosc;
	int wytrzymalosc;
public:
	Postac(
		string nazwa, 
		int sila = 10, 
		int zrecznosc = 4, 
		int wytrzymalosc = 20)
		: nazwa(nazwa),
		sila(sila),
		zrecznosc(zrecznosc),
		wytrzymalosc(wytrzymalosc)
	{
	}

	void walka(Postac &przeciwnik)
	{

	}

	void drukStats() const
	{
		cout << nazwa << endl
			<< "Sila: " << sila << endl
			<< "Zrecznosc: " << zrecznosc << endl
			<< "Wytrzymalosc: " << wytrzymalosc << endl << endl;
	}
};

int main()
{
	RandomNumbers::randomize();

	Postac janek
	(
		"janek",
		RandomNumbers::random(6, 10),
		RandomNumbers::random(4, 6),
		RandomNumbers::random(20, 30)
	);

	Postac karol
	(
		"karol",
		RandomNumbers::random(5, 8),
		RandomNumbers::random(8, 12),
		RandomNumbers::random(15, 22)
	);

	janek.drukStats();
	karol.drukStats();
}
